#!/bin/sh

podman-compose down
podman-compose stop
podman-compose build \
    --build-arg _UID=$(id -u) \
    --build-arg _GID=$(id -g) \
    --build-arg _UNAME=$(id -un) \
    --build-arg _GNAME=$(id -gn)
_UID=$(id -u) \
_GID=$(id -g) \
    podman-compose up
