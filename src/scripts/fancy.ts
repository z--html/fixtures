const fancy_spoilerRollback = (spoilerSummaryEl: HTMLLabelElement) => {
  const spoiler = spoilerSummaryEl.parentElement as HTMLDetailsElement;
  spoiler.classList.add('fancy-rollback');

  setTimeout(
    () => {
      spoiler.removeAttribute('open');
      spoiler.classList.remove('fancy-rollback');
    },
    parseInt(getComputedStyle(document.documentElement).getPropertyValue('--animation-duration')) + 5
  );
};

window.addEventListener(
  'load',
  () => {
    document.querySelectorAll('details.fancy-spoiler > .fancy-spoiler-opened-label').forEach(
      spoiler => {
        spoiler.addEventListener(
          'click',
          e => {
            e.preventDefault();
            fancy_spoilerRollback(e.target as HTMLLabelElement);
          }
        );
      }
    );
  }
);
