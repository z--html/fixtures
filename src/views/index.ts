// @ts-ignore
const pug = require('pug');

// Compile the source code
const compiledFunction = pug.compileFile('./src/templates/index.pug');

// Render a set of data
console.log(compiledFunction({
    title: 'My first PUG markup'
}));
