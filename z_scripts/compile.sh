#!/bin//sh

ASSETS_PUBLIC=${ASSETS_PUBLIC:-/usr/share/nginx/html}
ASSETS_TMP=${ASSETS_TMP:-/tmp/assets}

mkdir -p "${ASSETS_PUBLIC}"
mkdir -p "${ASSETS_TMP}/scripts"

scripts=$(ls ./src/scripts/ | cut -f1 -d'.')
for scr in ${scripts}; do
    tsc "./src/scripts/${scr}.ts" \
        --target esnext \
        --allowJs \
        --strict \
        --alwaysStrict \
        --removeComments \
        --outDir "${ASSETS_TMP}/scripts/" \
            || exit 1

    uglifyjs \
        "${ASSETS_TMP}/scripts/${scr}.js" \
        --compress --mangle \
        -o "${ASSETS_PUBLIC}/${scr}.js" \
            || exit 1
done

styles=$(ls ./src/styles/ | cut -f1 -d'.')
for sty in ${styles}; do
    sassc "./src/styles/${sty}.scss" \
        | minify --css \
        | tee "${ASSETS_PUBLIC}/${sty}.css" \
            >/dev/null 2>&1 \
                || exit 1
done

views=$(ls ./src/views/ | cut -f1 -d'.')
for vie in ${views}; do
    NODE_PATH=$(npm root -g) \
        ts-node "./src/views/${vie}.ts" \
            | minify --html \
            | tee "${ASSETS_PUBLIC}/${vie}.html" \
                >/dev/null 2>&1 \
                    || exit 1
done
